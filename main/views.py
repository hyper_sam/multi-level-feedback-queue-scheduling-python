from django.shortcuts import render

process_list = []
result_list = []


def index(request):
    # latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # context = {'latest_question_list': latest_question_list}
    new_process_name = request.POST.get('item_name')
    new_process_at = request.POST.get('arrive_time')
    new_process_st = request.POST.get('serve_time')

    print(type(new_process_at))
    if type(new_process_at) == str:
        new_process_at = int(new_process_at)
        new_process_st = int(new_process_st)

    new_process = Process(new_process_name, new_process_at, new_process_st)
    if request.POST.get('item_name') is not None:
        process_list.append(new_process)

    print(new_process)
    print(process_list)

    context = {
        "process_list": process_list
    }
    result_list.clear()

    # print(context["process_list"][0].name, context["process_list"][0].arrive_time)
    return render(request, 'main/index.html', context)


def add_form(request):
    context = {

    }
    return render(request, 'main/add_form.html', context)


def schedule_queue(request):
    # MLFQ START
    process_list0, process_list1, process_list2 = [], [], []
    for each in process_list:
        if each.serve_time <= 2:
            process_list0.append(each)
        elif each.serve_time <= 4:
            process_list1.append(each)
        else:
            process_list2.append(each)

    queue_list = []
    queue0 = Queue(0, process_list0)
    queue1 = Queue(1, process_list1)
    queue2 = Queue(2, process_list2)
    queue_list.append(queue0), queue_list.append(queue1), queue_list.append(queue2)
    # Using multi-level feedback queue scheduling algorithm, the first queue time slice is 2
    mfq = MulitlevedFeedbackQueue(queue_list, 2)
    mfq.scheduling()
    # MLFQ END

    # RR START
    # rr = RR(process_list, 1)
    # rr.scheduling()
    # RR END

    context = {
        "result_list": result_list
    }
    print(result_list[0].name)
    process_list.clear()

    return render(request, 'main/result_page.html', context)


class Process:
    def __init__(self, name, arrive_time, serve_time):
        self.name = name
        self.arrive_time = arrive_time
        self.serve_time = serve_time
        self.left_serve_time = serve_time
        self.finish_time = 0
        self.cycling_time = 0
        self.w_cycling_time = 0


class Queue:
    def __init__(self, level, process_list):
        self.level = level
        self.process_list = process_list
        self.q = 0

    def size(self):
        return len(self.process_list)

    def get(self, index):
        return self.process_list[index]

    def add(self, process):
        self.process_list.append(process)

    def delete(self, index):
        self.process_list.remove(self.process_list[index])


class RR:
    def __init__(self, process_list, q):
        self.process_list = process_list
        self.q = q

    def scheduling(self):
        process_list.sort(key=lambda x: x.arrive_time)  # according to .arrive_time Sort
        len_queue = len(self.process_list)  # The length of the process queue
        index = int(0)  # Indexes
        q = self.q  # Time slice
        running_time = int(0)  # Time already running

        # The scheduling loop
        while (True):
            if len_queue != 0:
                # The current process
                current_process = self.process_list[index % len_queue]
                # print(current_process)
                # Determine whether the current process has been completed
                if current_process.left_serve_time > 0:
                    # Calculate the completion time
                    # The service time is greater than or equal to the time slice , Then the completion time is + Time slice time The process is not over yet
                    # The service time is less than the time slice , Then the completion time is added to the original time of service
                    if current_process.left_serve_time >= q:
                        running_time += q
                        print(current_process.name, running_time, index)
                        current_process.left_serve_time -= q
                    else:
                        print('%s The service time is less than the current time slice ' % current_process.name)
                        running_time += current_process.left_serve_time
                        current_process.left_serve_time = 0
                        # Completed

                if current_process.left_serve_time == 0:
                    # Calculate the completion time
                    current_process.finish_time = running_time
                    # Calculate turnaround time
                    current_process.cycling_time = current_process.finish_time - current_process.arrive_time
                    # Calculate the turnaround time with rights
                    current_process.w_cycling_time = float(current_process.cycling_time) / current_process.serve_time
                    # Print
                    print('%s A completed process , The details are as follows ：' % current_process.name)
                    print(
                        ' Process name ：%s , Completion time ： %d , Turnaround time ：%d , Turnaround time with rights ： %.2f' % (
                            current_process.name, current_process.finish_time, current_process.cycling_time,
                            current_process.w_cycling_time))
                    result_list.append(current_process)

                    # eject
                    self.process_list.remove(current_process)
                    len_queue = len(self.process_list)
                    # After a process has completed its task ,index Go back first , Then add , To keep pointing to the next process that needs to be scheduled
                    index -= 1

            # index Regular increase
            index += 1

            # If there is no process in the queue, execution is complete
            if len(self.process_list) == 0:
                break

            # change index, Avoid it because index Greater than len, This leads to an error in taking the mold
            if index >= len(self.process_list):
                index = index % len_queue


class MulitlevedFeedbackQueue():
    def __init__(self, queue_list, q_first):
        self.queue_list = queue_list
        self.q_first = q_first

    def scheduling(self):
        q_list = self.queue_list  # Current queue set
        q_first = self.q_first  # The time slice of the first queue

        for i in range(len(q_list)):
            # Determine the time slice for each queue
            if i == 0:
                q_list[i].q = q_first
            else:
                q_list[i].q = q_list[i - 1].q * 2

            # Time slice execution starts from the first queue
            # First judge whether it is the last queue , The last queue is executed directly RR Scheduling algorithm
            # If it's not the last queue , After executing the current queue time slice, judge whether it is necessary to join the end of the next queue
            if i == len(q_list) - 1:
                print('************** Execute on the last queue RR Scheduling algorithm *************')

                # print(q_list[i].process_list[])
                # The last queue resets the arrival time
                for t in range(len(q_list[i].process_list)):
                    q_list[i].process_list[t].arrive_time = t
                rr_last_queue = RR(q_list[i].process_list, q_list[i].q)
                rr_last_queue.scheduling()
            else:
                currentQueue = q_list[i]

                index = int(0)
                while (True):
                    if index == currentQueue.size():
                        break

                    if currentQueue.get(index).left_serve_time > q_list[i].q:
                        currentQueue.get(index).left_serve_time -= q_list[i].q
                        print(' The first %d Queue time slice : %d' % (i, q_list[i].q))
                        print(
                            ' The process is not finished , Need to be added to the end of the next queue ： Process name ：%s ' % (
                                currentQueue.get(index).name))
                        # Throw the current process to the end of the next queue
                        q_list[i + 1].add(currentQueue.get(index))
                        index += 1
                    else:
                        print(' The service completes and pops up :', currentQueue.get(index).name)
                        currentQueue.get(index).left_serve_time = 0
                        result_list.append(currentQueue.get(index))
                        currentQueue.delete(index)

                    if index == currentQueue.size():
                        break
