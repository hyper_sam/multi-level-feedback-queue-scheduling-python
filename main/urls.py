from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add_form, name='add-form'),
    path('result-page/', views.schedule_queue, name="result-page"),
]